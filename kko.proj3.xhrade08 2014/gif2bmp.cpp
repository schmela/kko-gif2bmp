#include <cstdlib>
#include <iostream>
#include <string>
#include <istream>
#include <ostream>
#include <iterator>
#include <cstring>
#include <vector>
#include <bitset>

#include "gif2bmp.h"

using namespace std;

void file_write2B(FILE *f, int x) // zapise 2B do souboru
{
	fprintf(f, "%c%c", x, x>>8);
} 

void file_write4B(FILE *f, unsigned long x)  // zapise 4B do souboru
{
	fprintf(f, "%c%c%c%c", int(x), int(x>>8), int(x>>16), int(x>>24));
}

int gif2bmp(tGIF2BMP *gif2bmp, FILE *inputFile, FILE *outputFile)
{
	

	/// NACTENI VSTUPNIHO SOUBORU do stringu
	//////////////////////////////////////////////////////////////////////////

	string in;

	if (inputFile == NULL)
	{
		// nacte se komplet stdin --------------------------------------------

		// nepreskakuji se whitespaces
		std::cin >> std::noskipws;
		
		std::istream_iterator<char> it(std::cin); 
		std::istream_iterator<char> end;
		std::string results(it, end);
		in = results;
		gif2bmp->gifSize = in.length();
	}
	else
	{			
		// cely soubor nahraju do pameti --------------------------------------

		fseek (inputFile , 0 , SEEK_END); // zjistim delku souboru
		const int lSize = ftell (inputFile);
		rewind (inputFile);

		string mystring;
		mystring.resize(lSize);

		// nacteme soubor do stringu
		fread(&mystring[0], sizeof(char), (size_t)lSize, inputFile);
		fclose(inputFile);
	
		in = mystring;
		gif2bmp->gifSize = lSize;
	}

	/// HEADER 
	/////////////////////////////////////////////////////////////////////////////
	if (in.substr(0,6) != "GIF89a")
	{
		cerr << "not a GIF89a image - spatny header" << in.substr(0,6) << endl;
		exit(EXIT_FAILURE);
	}

	/// ROZMERY OBRAZKU
	/////////////////////////////////////////////////////////////////////////////
	uint16_t canvas_width, canvas_height;
	memcpy(&canvas_width,in.substr(6,2).c_str(),2);  
	memcpy(&canvas_height,in.substr(8,2).c_str(),2);  
	//cout << "canvas_width: " << canvas_width << endl;
	//cout << "canvas_height: " << canvas_height << endl;

	uint8_t logical_image_descriptor;
	memcpy(&logical_image_descriptor,in.substr(10,1).c_str(),1); 
	bool is_global_color_table = logical_image_descriptor & ((uint8_t)1 << 7);  // bit. AND 10000000
	bool is_8bit = logical_image_descriptor & ((uint8_t)7 << 4);  // bit. AND 01110000
	if (is_global_color_table && !is_8bit)
	{
		cerr << "not 8bit image" << endl;
		exit(EXIT_FAILURE);
	}

	// 10. byte bitove ANDujeme s hodnotou 00000111, priceteme jednicku a na toto cislo umocnime dvojku
	uint16_t global_color_table_size = 1 << (1 + (logical_image_descriptor & (uint8_t)7 ));

	uint8_t background_color_index;
	memcpy(&background_color_index,in.substr(11,1).c_str(),1);


	// pozice v "souboru"
	uint32_t CURRENT_BYTE = 13;

	/// GLOBAL COLOR TABLE
	/////////////////////////////////////////////////////////////////////////////
	std::vector <tRGB> global_color_table;
	if (is_global_color_table)
	{
		// na 13. bytu fixne zacina globalni tabulka barev
		uint32_t table_end = CURRENT_BYTE+(3*global_color_table_size);
		for (; CURRENT_BYTE < table_end; CURRENT_BYTE+=3)
		{
			tRGB color;
			memcpy(&color,in.substr(CURRENT_BYTE,3).c_str(),3); 
			global_color_table.push_back(color);
		}

		CURRENT_BYTE --; // za posledni kolo for cyklu


	}

	////
	// PODLE NORMY MUZE BYT ZA SEBOU VIC OBRAZKU
	// tedy vice poli GCE, image descriptor, local color table, image data
	// tady bereme v uvahu jenom jeden
	////

	// GRAPHIC CONTROL EXTENSION ?
	////////////////////////////////////////

	uint8_t extension_introducer, graphic_control_label, extension_length;
	memcpy(&extension_introducer,in.substr(CURRENT_BYTE+1,1).c_str(),1);
	memcpy(&graphic_control_label,in.substr(CURRENT_BYTE+2,1).c_str(),1);
	if ((extension_introducer == 0x21) && (graphic_control_label== 0xF9))
	{

		CURRENT_BYTE +=3; // doplnime to co jsme neposunuli precetli perd podminkou
		memcpy(&extension_length,in.substr(CURRENT_BYTE++,1).c_str(),1);

		// +1 za byte ukoncuji blok GCE
		CURRENT_BYTE += (extension_length+1);  // prozatim preskocime---------------------------------------------!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	}

	// IMAGE DESCRIPTOR (10B)
	////////////////////////////////////////

	uint8_t image_separator;
	uint16_t image_width, image_height;
	bool is_local_color_table;
	uint16_t local_color_table_size;
	memcpy(&image_separator,in.substr(CURRENT_BYTE++,1).c_str(),1);
	if (image_separator == 0x2C) 
	{
		CURRENT_BYTE += 4; // preskocime policka posouvajici tenhle obrazek v canvasu, pac uz se nepouzivaj
		
		memcpy(&image_width,in.substr(CURRENT_BYTE,2).c_str(),2);  
		CURRENT_BYTE+=2;
		memcpy(&image_height,in.substr(CURRENT_BYTE,2).c_str(),2);  
		CURRENT_BYTE+=2;

		// ZJISTUJEME ZDA EXISTUJE A JAK JE VELKA LOKALNI TABULKA BAREV
		uint8_t image_descriptor;
		memcpy(&image_descriptor,in.substr(CURRENT_BYTE++,1).c_str(),1);  
		is_local_color_table = image_descriptor & ((uint8_t)1 << 7);  // bit. AND 10000000
		// bitove ANDujeme s hodnotou 00000111, priceteme jednicku a na toto cislo umocnime dvojku
		local_color_table_size = 1 << (1 + (image_descriptor & (uint8_t)7 ));

	}
	else
	{
		cerr << "FAIL - image descriptor nenalezen" << endl;
		exit(1);
	}


	// LOCAL COLOR TABLE      3x local_color_table_size Bytu
	/////////////////////////////////////////////////////////

	std::vector <tRGB> color_table;
	uint32_t color_table_size;

	if (is_local_color_table)
	{
		color_table_size = local_color_table_size;

		uint32_t table_end = CURRENT_BYTE+(3*color_table_size);
		for (; CURRENT_BYTE < table_end; CURRENT_BYTE+=3)
		{
			tRGB color;
			memcpy(&color,in.substr(CURRENT_BYTE,3).c_str(),3); 
			color_table.push_back(color);
		}

		CURRENT_BYTE --; // za posledni kolo for cyklu

	}
	else
	{
		color_table_size = global_color_table_size;
		color_table = global_color_table;
	}

	
	// IMAGE DATA
	/////////////////////////////////////////////////////////

	uint32_t CURRENT_TMP = CURRENT_BYTE+1;

	// -- vykopirujem vsechny bloky k sobe do jednohos stringu
	string ALL_BLOCKS="";
	uint32_t bytes_in_blocks=0;
	uint32_t bytes_in_block_tmp=0; 
	memcpy(&bytes_in_block_tmp,in.substr(CURRENT_TMP++,1).c_str(),1); // pocet bytu v prvnim bloku

	bytes_in_blocks = bytes_in_block_tmp;
	do
	{
		// vykopirujeme blok do samostatneho stringu	
		string block = in.substr(CURRENT_TMP,bytes_in_block_tmp);

		ALL_BLOCKS.append(block);
	
		CURRENT_TMP += bytes_in_block_tmp;  
		
		// bytes in next block - kdyz se najde 0, tak obrazek konci
		memcpy(&bytes_in_block_tmp,in.substr(CURRENT_TMP++,1).c_str(),1);
		//cout << "next_block:" << (int)bytes_in_block_tmp << endl;

		bytes_in_blocks += bytes_in_block_tmp;

	} while (bytes_in_block_tmp != 0);

	ALL_BLOCKS.append("   "); // aby se mohlo cist i z posledniho bytu


	uint8_t min_code_size=0;   // minimalni delka kodu
	memcpy(&min_code_size,in.substr(CURRENT_BYTE++,1).c_str(),1);  

   
    // tabulka s kody
    vector<vector<int>> code_table;    

    // tabulka s kody jen pro tabulku barev - vracime se k ni po clear code
	vector<vector<int>> basic_code_table;    
	for (unsigned int i=0; i<color_table_size ; i++)
	{
		basic_code_table.push_back(vector<int> (1)={(int)i});
	}
	basic_code_table.push_back(vector<int> (0));  // clear code
	basic_code_table.push_back(vector<int> (0));  // end of image

	// vystup se vsemi pixely
	vector<int> index_stream;

	uint16_t delka_kodu = min_code_size + 1;

	uint16_t inblock_offset=0;

				
	uint32_t buffer;  // posuvne okno - jsou v nem ulozeny 4 aktualni byty
	uint8_t inbuf_offset=0;  // kolik bytu z ALL_BLOCKS jsme uz precetli
	memcpy(&buffer,ALL_BLOCKS.substr(inblock_offset,4).c_str(),4); // prvni 4 byty
	

	bool add_new_code=true; // aby se nepridaval novy kod po Clear code
	int previous_code=0;


	uint32_t bites_loaded = 0;  // kolik celkove bitu jsme uz precetli z ALL_BLOCKS

	bool clear = false;    // zda se ted nacetl Clear code

	///////////////////////////////////////////////
	// PRES VSECHNY KODY v ALL_BLOCKS
	/////////////////////////////////////////////

	while (1)	
	{

		uint32_t maska=0;
		
		// pokud je pocatecni delka kodu napr 9, tak vytvori masku 0000 0000 0000 0000 0000 0001 1111 1111 
		for (uint8_t i=0; i < delka_kodu; i++)
		{
			maska = maska << 1;
			maska = maska | 1;			
		}

		// masku posuneme tak, aby nam vybrala spravne bitu ze 4B okna
		maska = maska << inbuf_offset;

		uint32_t code= buffer & maska; // vysmaskujeme tak, ze zbude jen vyznamnych delka_kodu bitu 
		code = code >> inbuf_offset; // posuneme dopravu - tada, mame code barvy


		// KONTROLA CO SE NAM NACETLO
		///////////////////////////////////

		if (code == color_table_size)	// CLEAR CODE - smazat tabulku
		{
			code_table = basic_code_table;
			clear = true;
			add_new_code = false;
		}
		else if (code == (color_table_size + 1) ) // END OF IMAGE
		{
			break;
		}
		else if ( code >= code_table.size() ) // KOD NEEXISTUJE - pokud kod v tabulce jeste neni
		{
			// pridame do tabulky kodu indexy prechoziho kodu obohacene o jejich prvni prvek
			// to samy dame na vystup
			std::vector<int> tmp ( code_table.at(previous_code).begin(), code_table.at(previous_code).end() );
			tmp.push_back(tmp.front());			
			index_stream.insert(index_stream.end(),tmp.begin(),tmp.end());
			code_table.push_back(tmp);
			previous_code = code;
		}
		else  // KOD EXISTUJE - kod uz v tabulce je
		{ 
			// pridame indexy do index streamu
			index_stream.insert(index_stream.end(),code_table.at(code).begin(),code_table.at(code).end());    			

			// nedela se po clear-code
			if (add_new_code)
			{
				// pridame do tabulky kodu indexy prechoziho kodu obohacene o prvni prvek aktualnich indexu
				std::vector<int> tmp ( code_table.at(previous_code).begin(), code_table.at(previous_code).end() );
				tmp.push_back(code_table.at(code).front());
				code_table.push_back(tmp);
			}
			else
				add_new_code = true;

			previous_code = code;
		}


		// UPRAVY OFFSETU 

		inbuf_offset += delka_kodu;
		if (inbuf_offset >= 8)
		{
			inblock_offset += inbuf_offset/8;
			inbuf_offset = inbuf_offset % 8;
			if (inblock_offset < bytes_in_blocks)
				memcpy(&buffer,ALL_BLOCKS.substr(inblock_offset,4).c_str(),4); // posunuti okna
		}


		// ukoncovaci podminka cyklu - nacetli jsme potrebny pocet bitu
		bites_loaded += delka_kodu;
		if ((bites_loaded + delka_kodu) > (bytes_in_blocks*8))
			break;

		// zvyseni delky kodu pri naplneni tabulky pri 512, 1024, 2048, 4096 polozkach
		if ( code_table.size()  == (uint32_t)((1 << delka_kodu) )  )
		{
			delka_kodu++;
		}

		// pri clear-code je poterba jeste dalsi iteraci mi delku 12
		if (delka_kodu==13)
		{
			//cout << "DELKA KODU UZ JE 13 -> snizujeme na 12 " << endl;
			delka_kodu = 12;
		}

		// clear - code -> vycistime tabulku
		if (clear)
		{
			code_table = basic_code_table;
			delka_kodu = min_code_size + 1;
			clear = false;
		}
			


	} // end while(1)


	// ZAPIS DO BMP
	///////////////////////////////////////////////////////

	if (outputFile != NULL) // do souboru -o file
	{
		fprintf(outputFile,"BM");  // magic number for .bmp files
		file_write4B(outputFile, 54+ 3*index_stream.size());  // file size
		file_write4B(outputFile, 0);  // reserved
		file_write4B(outputFile, 54);  // offset to start of image (no palette)
		file_write4B(outputFile, 40);  // info header size
		file_write4B(outputFile, image_width);  // image size in pixels
		file_write4B(outputFile, image_height);
		file_write2B(outputFile, 1);  // image planes
		file_write2B(outputFile, 24);  // output bits per pixel
		file_write4B(outputFile, 0);  // no compression
		file_write4B(outputFile, 3*index_stream.size());  // image size in bytes
		file_write4B(outputFile, 2835);  // x pixels per meter
		file_write4B(outputFile, 2835);  // y pixels per meter
		file_write4B(outputFile, 0);  // colors
		file_write4B(outputFile, 0);  // important colors

		int padd = 4 - (( image_width*3 ) % 4 ); // padding pro 1 radek
		for (int i=image_height-1; i>=0; i--)
		{
			for (int j=0; j<image_width; j++)
			{
				// poradi BGR
				putc(color_table.at(index_stream.at(i*image_width + j)).blue, outputFile);
				putc(color_table.at(index_stream.at(i*image_width + j)).green, outputFile);
				putc(color_table.at(index_stream.at(i*image_width + j)).red, outputFile);
			}

			for (int p=0; p < padd; p++)
			{
				// tiskn paddingu
				fputc (0,outputFile);
			}
			

		}

		fclose(outputFile);
  	}
  	else // zapis do STDOUT
  	{
  		// hlavicky
  		fprintf(stdout,"BM");  // magic number for .bmp files
		file_write4B(stdout, 54+ 3*index_stream.size());  // file size
		file_write4B(stdout, 0);  // reserved
		file_write4B(stdout, 54);  // offset to start of image (no palette)
		file_write4B(stdout, 40);  // info header size
		file_write4B(stdout, image_width);  // image size in pixels
		file_write4B(stdout, image_height);
		file_write2B(stdout, 1);  // image planes
		file_write2B(stdout, 24);  // output bits per pixel
		file_write4B(stdout, 0);  // no compression
		file_write4B(stdout, index_stream.size()*3);  // image size in bytes
		file_write4B(stdout, 2835);  // x pixels per meter
		file_write4B(stdout, 2835);  // y pixels per meter
		file_write4B(stdout, 0);  // colors
		file_write4B(stdout, 0);  // important colors

		int padd = 4 - (( image_width*3 ) % 4 ); // padding pro 1 radek
		for (int i=image_height-1; i>=0; i--)
		{
			for (int j=0; j<image_width; j++)
			{
				// poradi BGR
				putc(color_table.at(index_stream.at(i*image_width + j)).blue, stdout);
				putc(color_table.at(index_stream.at(i*image_width + j)).green, stdout);
				putc(color_table.at(index_stream.at(i*image_width + j)).red, stdout);
			}

			// tiskn paddingu
			for (int p=0; p<padd; p++)
			{
				putc (0,stdout);
			}

		}    
  	}

  	gif2bmp->bmpSize =  54+ (3*index_stream.size());

	return 1;
}

