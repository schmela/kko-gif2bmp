#include <iostream>
#include <string>
#include <fstream>
#include <getopt.h>

#include "gif2bmp.h"

using namespace std;

bool log_flag = false; // jestli se ma logovat do souboru
bool i_flag = false;   // jestli je dan vstupni soubor
bool o_flag = false;   // jestli je dan vystupni soubor
string logFile, inputFile, outputFile;

void help_and_exit()
{
	cout << "KKO projekt - GIF2BMP" << endl;
	cout << "=====================" << endl;
	cout << endl;
	cout << "gif2bmp -i <ifile> -o <ofile> [-l <logfile>]" << endl;
	cout << "--------------------------------------------" << endl;
	cout << "prevede vstupni GIF obrazek <ifile> na obrazek formatu BMP a ulozi jej do souboru <ofile>. Popr zapise do soboru <logfile> vypis o prubehu." << endl;
	cout << endl;
	cout << "gif2bmp -h" << endl;
	cout << "----------" << endl;
	cout << "vypise tuto napovedu a ukonci se" << endl;
	exit(0);
}

// vytiskne puvodni zpravu na STDOUT nebo do souboru -l log
void vystupniZprava(int64_t uncodedSize, int64_t codedSize)
{

  if (log_flag)
  {
  	ofstream logFileStream(logFile);

  	logFileStream << "login = xhrade08" << endl;
  	logFileStream << "uncodedSize = " << uncodedSize << endl;
  	logFileStream << "codedSize = " << codedSize << endl;

  	logFileStream.close();
  }
  else
  {
    cout << "login = xhrade08" << endl;
    cout << "uncodedSize = " << uncodedSize << endl;
    cout << "codedSize = " << codedSize << endl;
  }
}

// parsovani argumetu
void parse_arguments(int argc, char *argv[]){

  int c;

  while ((c = getopt (argc, argv, "i:o:l:h")) != -1){
   switch (c){
      case 'i':{
          inputFile = optarg;
          i_flag = true;
          break;
      }
      case 'o':{
          outputFile = optarg;
          o_flag = true;
          break;
      }      
      case 'l':{
          logFile = optarg;
          log_flag = true;
          break;
      }               
      default:{
      	help_and_exit();
      }
   }    
 }
}

int main(int argc, char *argv[])
{

  /// kontrola parametru --------------------------------------------------
  parse_arguments (argc, argv);

  /// otevreni souboru ----------------------------------------------------
  FILE *fin = NULL;
  if ((i_flag) && ((fin = fopen(inputFile.c_str(), "rb"))== NULL) )
    cerr << "Nelze otevrit soubor " << inputFile << endl;

  FILE *fout = NULL;
  if ((o_flag) && ((fout = fopen(outputFile.c_str(), "wb")) == NULL))
    cerr << "Nelze otevrit soubor " << outputFile << endl;

  /// samotny prevod a vypis vysledku -------------------------------------
  tGIF2BMP vysledky_prevodu;
  gif2bmp(&vysledky_prevodu, fin, fout);
  vystupniZprava(vysledky_prevodu.bmpSize, vysledky_prevodu.gifSize);

  return EXIT_SUCCESS;
}
