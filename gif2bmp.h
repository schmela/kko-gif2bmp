#ifndef GIF2BMP_H
#define GIF2BMP_H

#include <sys/types.h>
#include <stdio.h>

typedef struct
{
	int64_t bmpSize;
	int64_t gifSize; // puvodne int64_t long gifSize; - ale hlasilo to warning
} tGIF2BMP;


typedef struct
{
	uint8_t red,green,blue;
} tRGB;

/* gif2bmp – záznam o převodu
inputFile – vstupní soubor (GIF)
outputFile – výstupní soubor (BMP)
návratová hodnota – 0 převod proběhl v pořádku, -1 při převodu došlo k
chybě, příp. nepodporuje daný formát GIF */
int gif2bmp(tGIF2BMP *gif2bmp, FILE *inputFile, FILE *outputFile);

#endif
