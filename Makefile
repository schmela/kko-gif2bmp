CPP          =  g++
CPPFLAGS     = -std=c++11 -pedantic -Wextra -g -lstdc++ -static-libstdc++ -static-libgcc

all: LD_LIBRARY_PATH=/usr/local/lib64 gif2bmp

gif2bmp: main.o gif2bmp.o
	$(CPP) $(CPPLAGS) -o gif2bmp main.o gif2bmp.o

main.o: main.cpp gif2bmp.cpp gif2bmp.h
	$(CPP) $(CPPFLAGS) -c main.cpp

gif2bmp.o: gif2bmp.cpp gif2bmp.h
	$(CPP) $(CPPFLAGS) -c gif2bmp.cpp

clean:
	rm *.o gif2bmp

pack:
	mkdir kko.proj3.xhrade08 
	cp *.cpp *.h Makefile ./dokumentace/gif2bmp.pdf kko.proj3.xhrade08/
	zip -r kko.proj3.xhrade08.zip ./kko.proj3.xhrade08/*
	rm -r kko.proj3.xhrade08/