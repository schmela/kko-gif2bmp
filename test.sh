#!/bin/bash


rm ./test/*.bmp
rm ./test/*.log

#!/bin/bash
FILES=./test/*.gif
for infile in $FILES
do
  echo "Processing $infile"
  echo "==================================="
  
  filename=$(basename "$infile")

  ./gif2bmp -i $infile -o "./test/"${filename}".a.bmp" -l ${infile}".a.log"
  if (($? == 0)); then
	cat ${infile}".a.log"  
  fi 
  ./gif2bmp -i $infile > "./test/"${filename}".b.bmp" -l ${infile}".b.log"
  if (($? == 0)); then
	cat ${infile}".b.log"  
  fi
  ./gif2bmp < $infile -o "./test/"${filename}".c.bmp" -l ${infile}".c.log"
  if (($? == 0)); then
	cat ${infile}".c.log"  
  fi
  ./gif2bmp < $infile > "./test/"${filename}".d.bmp" -l ${infile}".d.log"
  if (($? == 0)); then
	cat ${infile}".d.log"  
  fi
  
  echo " "
  #cat $f
done